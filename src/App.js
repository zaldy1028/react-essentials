import logo from './logo.svg';
import './App.css';
import restaurant from "./maxresdefault.jpg";

function Header(props) {
  return (
    <header>
      <h1>{props.name} Kitchen</h1>
    </header>
  );
}

function Main(props) {
  return (
    <section>
      <p>We serve the most {props.adjective} food around.</p>
      <img src={restaurant} height={300} alt="I don't know" />
      <ul style={{ textAlign: 'left'}}>
        {props.dishes.map((dish) => (
          <li key={dish.id}>{dish.title}</li>
        ))}
      </ul>
    </section>
  );
}

function Footer(props) {
  return(
    <p>It's true! Even today in {props.year}</p>
  );
}

const dishes = [
  "Toasted sandwich spread variety",
  "Biko with langka special",
  "Hash browns",
  "Ramly square burger"
];

const dishObjects = dishes.map((dish, i) => ({ id: i, title: dish }));

function App() {
  return (
    <div className="App">
      <Header name="Ate Jen's" />
      <Main adjective="magnanimous" dishes={dishObjects}/>
      <Footer year={new Date().getFullYear()} />
    </div>
  );
}

export default App;
